<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.onelogin.*,com.onelogin.saml.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SAML Assertion Page</title>
</head>
<body>
<%
  String certificateS ="MIIECzCCAvOgAwIBAgIUfyL63480lm74gJ5wQbG7ZcFGSCUwDQYJKoZIhvcNAQEFBQAwVDELMAkGA1UEBhMCVVMxDTALBgNVBAoMBENsZW8xFTATBgNVBAsMDE9uZUxvZ2luIElkUDEfMB0GA1UEAwwWT25lTG9naW4gQWNjb3VudCA4NzQ0MzAeFw0xNjA3MTEwOTUxMTRaFw0yMTA3MTIwOTUxMTRaMFQxCzAJBgNVBAYTAlVTMQ0wCwYDVQQKDARDbGVvMRUwEwYDVQQLDAxPbmVMb2dpbiBJZFAxHzAdBgNVBAMMFk9uZUxvZ2luIEFjY291bnQgODc0NDMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCsD0xnyKuQ1KaIgn3m/NqO2qgNfbsxGa7DavRi2numfPqcosoWdTP94Z0WaGcWNGheNV7T4ERRPrH6G33ER/oX2DpQ3zEIO771tkLkHBI0vLVYRTMDLr66mVSzBJbKLgyGULvPIgsQCWQ+I3e4VroXpNEL++cjWQIJZq9Fe8/oWahjC5DiK3k3IxHwVMtxM07QrHZ92Hm69tIXPJZoTpYG7k+kn1B1QxkSo8tuMQSP22/O6hgAWFrz9FUG4/aPYngNBYl2ADrviAjlUe3h8RzVGGzJv+3zSRRQ9rSmy0+tbJ9nmZA0T3a02ZLvA4I2yAN+biFnr76HnF7H/twnNXibAgMBAAGjgdQwgdEwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUht2oOnGjqshtvSl4phSNcUEIQbwwgZEGA1UdIwSBiTCBhoAUht2oOnGjqshtvSl4phSNcUEIQbyhWKRWMFQxCzAJBgNVBAYTAlVTMQ0wCwYDVQQKDARDbGVvMRUwEwYDVQQLDAxPbmVMb2dpbiBJZFAxHzAdBgNVBAMMFk9uZUxvZ2luIEFjY291bnQgODc0NDOCFH8i+t+PNJZu+ICecEGxu2XBRkglMA4GA1UdDwEB/wQEAwIHgDANBgkqhkiG9w0BAQUFAAOCAQEAXHnMx0FFaPYYipxiwBAD8/KDEHKjAchsEo6oePWZQMtWe058QZy8gTgvKlGaQrHuegzgodbrRCyPm2/qyeqDtu43sPwHzmZCjtklIsN62K2llJgLGuauMT+PwDyv/840Bme7HWwGICi/NaJIcBprbHxaGtPAqknA6eq78q1dNj2NyOVaicaOjGBKOQ/vu7Bl/MYNA0fSbcP0URiReb7s+kSwB0ooEalKJCCaTWkLDAW5pAs8Aec8U1Y9zQ3GwOJ2MU8p3iWYBHlg8p88FqA5blKOijiQ7DtmzSZ6MQWRu7HaycEMCsmP1TuWgUib7KuDqRGBHqXRixqRlveQy8LgCg==";

  // user account specific settings. Import the certificate here
  AccountSettings accountSettings = new AccountSettings();
  accountSettings.setCertificate(certificateS);

  Response samlResponse = new Response(accountSettings,
                                      request.getParameter("SAMLResponse"),
                                      request.getRequestURL().toString());

  if (samlResponse.isValid()) {

    // the signature of the SAML Response is valid. The source is trusted
  	java.io.PrintWriter writer = response.getWriter();
  	writer.write("OK! for this session");
  	String nameId = samlResponse.getNameId();
  	writer.write(nameId);
  	writer.flush();

  } else {

    // the signature of the SAML Response is not valid
  	java.io.PrintWriter writer = response.getWriter();
  	writer.write("Failed with error \n");
    writer.write(samlResponse.getError());
  	writer.flush();

  }
%>
</body>
</html>
